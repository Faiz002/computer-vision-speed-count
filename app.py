from flask import Flask ,render_template ,request
from speedtest import track_speed_detect
from flask import Flask, request, jsonify, send_file
import os
from werkzeug.utils import secure_filename


app = Flask(__name__)

# Web server gateway interface
UPLOAD_FOLDER = 'uploads'
if not os.path.exists(UPLOAD_FOLDER):
    os.makedirs(UPLOAD_FOLDER)

ALLOWED_EXTENSIONS = {'mp4'}
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/')
def index():
    return render_template('./templates/layout.html')

@app.route('/analyze_video', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return jsonify({"error": "No file part"}), 400
    file = request.files['file']

    if file.filename == '':
        return jsonify({"error": "No selected file"}), 400
    
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        filepath = os.path.join(UPLOAD_FOLDER, filename)
        print('file',filepath)
        file.save(filepath)
        output_video_path = 'speed_estimation.avi'

        vehicle_count, moving_vehicle_count ,car_counts ,truck_counts = track_speed_detect(filepath)
        return jsonify({
            "message": "File uploaded successfully",
            "filename": filename,
            "vehicle_count": vehicle_count,
            "moving_vehicle_count": moving_vehicle_count,
            "car_counts":car_counts,
            "truck_counts" :truck_counts,
            "Average_speed":"please Watch the saved speed_estimation.avi video",
            "video_path": output_video_path
        }), 200
    else:
        return jsonify({"error": "File type not allowed"}), 400
    

@app.route('/download', methods=['GET'])
def download_video():
    video_name = request.args.get('name')
    print(video_name)
    return send_file(video_name, as_attachment=True , download_name = "dtfvgzbhunjim.avi")






if __name__ == "__main__":
    app.run(debug=True)

from ultralytics import YOLO
from ultralytics.solutions import speed_estimation
from ultralytics.utils.plotting import Annotator, colors
from ultralytics.solutions import object_counter
import ultralytics
import cv2

def track_speed_detect(video_path):
    model = YOLO("yolov8n-seg.pt")
    names = model.model.names
    vehicles_class = [1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0]
    cap = cv2.VideoCapture(video_path)
    assert cap.isOpened(), "Error reading video file"
    w, h, fps = (int(cap.get(x)) for x in (cv2.CAP_PROP_FRAME_WIDTH, cv2.CAP_PROP_FRAME_HEIGHT, cv2.CAP_PROP_FPS))

    vehicles_id = []
    # Reducing Speed in speed_estimation.avi ti visualise clearly
    slow_down_factor = 8 
    # Video writer 
    slow_fps = fps / slow_down_factor
    video_writer = cv2.VideoWriter("speed_estimation.mp4",cv2.VideoWriter_fourcc(*'mp4v'),slow_fps,(w, h))

    #Line Region
    line_pts = [(0, 360), (2000,360)]
    lines = line_pts 

    # Init speed-estimation obj
    # estimate_speed(self, im0, tracks, region_color=(255, 0, 0))
    speed_obj = speed_estimation.SpeedEstimator()

    # Init object_counter obj
    # counter(self, im1, tracks, region_color=(255, 0, 0))
    counter = object_counter.ObjectCounter()


    #########set args for counter #################
    counter.set_args(view_img = False,
        view_in_counts = False,
        view_out_counts = False,
                reg_pts=lines,
                classes_names=names,
                draw_tracks=True)

    #########set args for speed estimator #################
    speed_obj.set_args(reg_pts=lines,
                    spdl_dist_thresh=100,
                    names=names,
                    view_img=True,
                    )

    car_counts = 0
    truck_counts = 0
    tracked = []
    last_count = 0
    while cap.isOpened():
        #im0 is for speed estimation
        success, im0 = cap.read()
        #im1 is for Counts
        success1,im1 = cap.read()
        if not success:
            print("Video frame is empty or video processing has been successfully completed.")
            break
        
        #results is for speed estimation tracking
        results = model.track(im0, persist=True, show=False,classes=vehicles_class)
        #results_count is for counting tracking
        results_count = model.track(im0, persist=True, show=False,classes=vehicles_class)

        if results[0].boxes.id is not None and results[0].masks is not None:
            x1, y1, x2, y2, score,_,class_id= results[0].boxes.data.cpu().tolist()[0]
            masks = results[0].masks.xy
            track_ids = results[0].boxes.id.int().cpu().tolist()
            speed_info = results[0].speed
            clss = results[0].boxes.cls.cpu().tolist()
            
            #im0 start estimate_speed
            im0 = speed_obj.estimate_speed(im0, results)

            #im1 start_counting
            im1 = counter.start_counting(im1, results)

            #print('here',vars(ultralytics.solutions.object_counter.ObjectCounter))
            for i, r in enumerate(results_count):
                for index, box in enumerate(r.boxes):
                    tracker_id = box.id
                    
            for obj in results_count:
                if counter.out_counts+counter.in_counts > last_count:
                    if tracker_id not in tracked:
                        if obj.boxes[0].cls == 2:  # Check if the class is car
                            car_counts += 1
                            tracked.append(tracker_id)  
                        elif obj.boxes[0].cls == 8:  # Check if the class is truck
                            truck_counts += 1
                            tracked.append(tracker_id)                                
            last_count = counter.out_counts+counter.in_counts 
            video_writer.write(im0)
            
        #f cv2.waitKey(1) & 0xFF == ord('q'):
            #   break

    cap.release()
    video_writer.release()

    return counter.in_counts ,counter.out_counts ,car_counts,truck_counts

#track_speed_detect("vehicle.mp4")


